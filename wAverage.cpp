/*
 This program populates a list based on a block size
 then sums each index based on an increasing
 multiplier. So higher indices are worth more and lower less.  So
 a list with larger numbers in higher indices will sum a larger number.
 This will correspond to the brighter or darker spectrum of a sound, or
 anything for that matter, as long as the numbers coming in correspond to 
 a frequency spectrum in bins.  
 
 This program assumes the numbers it receives are in order and that 
 the block size is a power of 2.  It also assumes that it is receiving
 frequency bin information.
 
 It will also detect if there is a harmonic present, e.g two bins of 
 relative strength in a 1/2 ration.  If not it changes a scalar in the 
 average function so that "noisier" sounds will yield a larger number. 
 */

#include "wAverage.h"
#include <iostream>
#include <list>
#include <cstdlib>
#include <time.h>

#define MINBLOCKSIZE 512
#define MINBLOCKMAG .1

// *****************************************
// constructor

wAverage::wAverage()
{
	
}

// deconstructor
wAverage::~wAverage()
{
	
}

// *****************************************
// weighted average
double wAverage::returnAverage(double listArray[])
{
	
	
	
	int bin = 1; // index in list, aka bin number in the frequency spectrum
	double average = 0;
	int binCount = 0;
	bool binFull = 0;
	int *binArray;
	int scaleMultiplier = 0;
	bool harmonicDetected = 0;
	
	for (int i = 0; i < blocksize; i++) 
	{
		// TODO:  figure out what is the best # to use in MINBLOCKMAG, or use another average, max/min or whatever.
		if (listArray[i] > MINBLOCKMAG) // check how many #s are worthy of our time
		{
			binCount++;
		}
		
	}
	
	binArray = new int [binCount]; // allocate memory to binArray
	
	int newBinCount = 0;
	
	for (int i = 0; i < blocksize; i++) 
	{
		
		if (listArray[i] > MINBLOCKMAG) // check if the bin is above .1, if yes increment
			// the binCount  
		{
			binFull = 1;
			
		}
		else 
		{
			binFull = 0;
		}
		
		
		
		if (binFull) 
		{
			binArray[newBinCount] = bin; // store the bin that is "full" in an array.
			newBinCount++;
		}
		
		bin++;
		
		
	}
	

	
	for (int i = 0; i < binCount; i++) // On^2 where n is always < .5*blocksize
	{
		
		for (int j =1; j < binCount; j++)
		{	
			if (blocksize >= MINBLOCKSIZE)  // blocksize needs to be at least 1024/2
			{	
				
				//has a harmonic, probably a pitch
				
				if ( (binArray[i]) * 2 == binArray[j] && binArray[j] != 0)
				{									 
					harmonicFrequency = binArray[i] * (sampleRate/(blocksize*2));
					harmonicDetected = 1;
				}
				
			}
		} // for j
		
	} // for i
	
	if (harmonicDetected)
	{
		scaleMultiplier = 1;
	}
	else 
	{
		scaleMultiplier = 10;
	}
	
	
	
	bin = 1; // reset bin to 1 
	
	for (int j = 0; j < blocksize; j++) 
	{
		average += listArray[j] * (bin * scaleMultiplier);
		//std::cout << listArray[j] * bin << std::endl;
		bin++;
		
	}
	
	// output the number as an average.
	
    return average/blocksize;
	
	delete [] binArray;
}

// **********************************************************************************************
// function to check if block size is power of 2, this is to assure that the proper # of elements
// is being passed from one place to another.  This would affect the resulting average.

bool wAverage::checkBlock(int var)
{
	bool blockGood;
	bool blockSize;
	
	if ( !var || var & (var-1) ) // power of 2?
	{
		blockGood = 0; // do nothing for now
	}
	else
	{
		blockGood = 1;
	}	
	
	
	if(!blockGood && (var * 2) < MINBLOCKSIZE) // is blocksize > 512?
	{
		blockSize = 0;
	}
	else 
	{
		blockSize = 1;
	}
	
	
	return blockSize;
	
	// for now continue even if blocksize is bad, if it becomes a problem another routine is necessary
}


// ***************************************** 
//for now just populate with random numbers

double wAverage::populateList(void) 
{
	
	double myNumber = rand() % 1000;
	return myNumber/1000;
}


// *****************************************
// mutator block size
void wAverage::setBlocksize(int blck)
{
	
	blocksize = blck/2; // take half of block size b/c rest will be zeros due to Nyquist's stupid theory
}


// *****************************************
// accessor block size
int wAverage::getBlocksize()
{
	
	return blocksize;
}

// *****************************************
// mutator sample rate
void wAverage::setSampleRate(float sr)
{
	
	sampleRate = sr;
	
}

// *****************************************
// accessor sample rates
float wAverage::getSampleRate()
{
	
	return sampleRate;
}

// *****************************************
// mutator harmonic frequency
void wAverage::setHarmonicFrequency(double hf  )
{
  	
	harmonicFrequency = hf;
	
}

double wAverage::getHarmonicFrequency()
{
	
	return harmonicFrequency;
}
