//
//  main.c
//  PdFloatOrder
//  Reorders four floats from low to high
//  Created by Robert Esler on 12/3/14.
//  Copyright (c) 2014 Robert Esler. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "m_pd.h"

static t_class *reorder_class;

typedef struct _reorder {
    t_object  x_obj;
    t_float num1;
    t_float num2;
    t_float num3;
    t_float num4;
    t_outlet *outlet1;
    t_outlet *outlet2;
    t_outlet *outlet3;
    t_outlet *outlet4;
} t_reorder;

void reorder_bang(t_reorder *x)
{
    
}

int cmpfunc (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}

void reorder_main(t_reorder *x, t_floatarg f1) {
    
    x->num1 = f1;
    t_float values[4] = {x->num1, x->num2, x->num3, x->num4};
    
    qsort(values, 4, sizeof(t_float), cmpfunc);
    //should be low to high
    outlet_float(x->outlet1, values[0]);
    outlet_float(x->outlet2, values[1]);
    outlet_float(x->outlet3, values[2]);
    outlet_float(x->outlet4, values[3]);
}

void *reorder_new(t_symbol *s, int argc, t_atom *argv)
{
    t_reorder *x = (t_reorder *)pd_new(reorder_class);
    floatinlet_new(&x->x_obj, &x->num2);
    floatinlet_new(&x->x_obj, &x->num3);
    floatinlet_new(&x->x_obj, &x->num4);
    
    x->outlet1 = outlet_new(&x->x_obj, &s_float);
    x->outlet2 = outlet_new(&x->x_obj, &s_float);
    x->outlet3 = outlet_new(&x->x_obj, &s_float);
    x->outlet4 = outlet_new(&x->x_obj, &s_float);
    
    // initialize values
    x->num1 = 0;
    x->num2 = 0;
    x->num3 = 0;
    x->num4 = 0;
    
    return (void *)x;
}

void reorder_setup(void) {
    reorder_class = class_new(gensym("reorder"),
                                 (t_newmethod)reorder_new,
                                 0, sizeof(t_reorder),
                                 CLASS_DEFAULT, 0);
    class_addbang(reorder_class, reorder_bang);
    class_addfloat(reorder_class, reorder_main);
}