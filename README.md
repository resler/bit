# README #

These are all examples of code I have used to create Pure Data externals.  The benefit of prototyping code using Pd is that you get the benefit of an easy access DSP library of objects and a handy GUI all neatly scheduled for high quality audio.  
  If you plan to use this code you should know how to compile dynamic libraries as Pd dynamically links these externals.  See: http://iem.at/pd/externals-HOWTO/HOWTO-externals-en.html
  Some of the source files go together, such as binavg~.cpp, wAverage.h, wAverage.cpp.  Others are just single files.  If trying to compile these into objects make sure to download the Pd source code first and copy the m_pd.h file into your bundle.  
  This repository is really just to display some of the code I've developed over the years.  It isn't meant to function as source for a larger scale project.  See Pd++ for a more complete audio synthesis library.

# License and other information: #****
This library is released under the same license as Pure Data. See the license information below.
This software is copyrighted by Robert Esler. The following terms (the "Standard Improved BSD License") apply to all files associated with the software unless explicitly disclaimed in individual files:
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.