//
//  bitcrusher.cpp
//  bitcrusher
//
//  Created by Robert Esler on 7/31/14.
//  Copyright (c) 2014 Robert Esler. All rights reserved.
//
//  This project creates an variable bit oscillator.
//
//  main.cpp
//  eigthBit
//
//  Created by Robert Esler on 7/29/14.
//  Copyright (c) 2014 Robert Esler. All rights reserved.
//

#include "m_pd.h"
#include <cmath>
#define BITDEPTH 16

#ifdef __cplusplus
extern "C" {
#endif
    
    static t_class *bitcrusher_tilde_class;
    
    typedef struct _bitcrusher_tilde {
        t_object  x_obj;
        t_float bitDepth;
        t_float freq;
        t_sample theOutput;
        t_float samplerate;
        
        
        t_inlet *x_in2;
        t_outlet*x_out;
    } t_bitcrusher_tilde;
    
    
    t_int *bitcrusher_tilde_perform(t_int *w)
    {
        t_bitcrusher_tilde *x = (t_bitcrusher_tilde *)(w[1]);
        t_sample *in1 = (t_sample *)(w[2]);
        t_sample  *out =    (t_sample *)(w[3]);
        int          n =           (int)(w[4]);
        
        
        double temp, fracPart, intPart;
        
        if(x->bitDepth == 0)
            x->bitDepth = 1;
        
        while (n--)
        {
            temp = *in1++;
            temp *= x->bitDepth;
            
            fracPart = modf(temp, &intPart);
            
            x->theOutput = intPart * (1/x->bitDepth);
            
            *out++ = x->theOutput;
            
        }
        
        return (w+5);
    }
    
    void bitcrusher_tilde_dsp(t_bitcrusher_tilde *x, t_signal **sp)
    {
        dsp_add(bitcrusher_tilde_perform, 4, x,
                sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
    }
    
    void bitcrusher_tilde_free(t_bitcrusher_tilde *x)
    {
        inlet_free(x->x_in2);
        outlet_free(x->x_out);
    }
    
    
    void *bitcrusher_tilde_new(t_floatarg f)
    {
        t_bitcrusher_tilde *x = (t_bitcrusher_tilde *)pd_new(bitcrusher_tilde_class);
        
        
        x->bitDepth = f;
        x->samplerate = sys_getsr();
        x->theOutput = 0;
        
        x->x_in2 = floatinlet_new(&x->x_obj, &x->bitDepth);
        x->x_out = outlet_new(&x->x_obj, &s_signal);
        
        return (void *)x;
    }
    
    void bitcrusher_tilde_setup(void) {
        bitcrusher_tilde_class = class_new(gensym("bitcrusher~"),
                                         (t_newmethod)bitcrusher_tilde_new,
                                         (t_method)bitcrusher_tilde_free, sizeof(t_bitcrusher_tilde),
                                         CLASS_DEFAULT,
                                         A_DEFFLOAT, A_DEFFLOAT, 0);
        
        class_addmethod(bitcrusher_tilde_class,
                        (t_method)bitcrusher_tilde_dsp, gensym("dsp"),A_DEFFLOAT, 0);
        // class_addfloat(bitcrusher_tilde_class, (t_method)bitcrusher_tilde_float);
        class_sethelpsymbol(bitcrusher_tilde_class, gensym("help-bitcrusher~"));
        
        CLASS_MAINSIGNALIN(bitcrusher_tilde_class, t_bitcrusher_tilde, freq);
    }
    
#ifdef __cplusplus
}
#endif

