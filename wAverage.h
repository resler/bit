/*
 *  wAverage.h
 *  weightedAverage
 *
 *  Created by Robert Esler on 2/23/12.
 *  Copyright 2012 All rights reserved.
 *
 */

#ifndef WAVERAGE_H
#define WAVERAGE_H


class wAverage {

public:
	
	wAverage();
	~wAverage();
	double returnAverage(double[] );
	double populateList(void );
	void setBlocksize(int );
	int getBlocksize();
	void setSampleRate(float );
	float getSampleRate();
	void setHarmonicFrequency(double );
	double getHarmonicFrequency();
	bool checkBlock(int );
	
	
private:
	
	unsigned int blocksize;
	float sampleRate;
	double harmonicFrequency;

};

#endif