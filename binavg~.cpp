/*
 *  binavg~.cpp
 *  uses wAverage.cpp
 *
 *  binavg~ uses a weighted average for frequency bin information via FFT
 *  This external can be used to determine spectral color
 *  If higher frequencies are more pervasive than a higher average
 *  Also, the more noise present the higher the average.
 * refer to http://iem.at/pd/externals-HOWTO/node1.html
 *
 */


#include "m_pd.h"
#include "wAverage.h"

// avoid any c++ function mangling, probably won't be a problem anyway
#ifdef __cplusplus 
extern "C" {
#endif


static t_class *binavg_tilde_class;

typedef struct _binavg_tilde {
	t_object  x_obj;
	t_float f_binavg;
	t_float f_harmonic;
	t_sample f;
	t_outlet *f_ctrlOut, *f_harmonicOut;
} t_binavg_tilde;

t_int *binavg_tilde_perform(t_int *w)
{
	wAverage avg;
	
	t_binavg_tilde *x = (t_binavg_tilde *)(w[1]);
	t_sample  *in1 =    (t_sample *)(w[2]);
	t_float	ctrlOut = x->f_binavg;
	t_float harmonicOut = x->f_harmonic;
	int          n =           (int)(w[3]);
	
	
	float sampleRatePassToClass = sys_getsr(); // get sample rate
	
	avg.setSampleRate(sampleRatePassToClass); // set sample rate
	avg.setBlocksize(n); // set block size
	//post("sr = %i", avg.getBlocksize());
	
	
	double inputArray[avg.getBlocksize()]; // create an array to hold the input stream (*in1);
	
	for (int i = 0; i < avg.getBlocksize(); i++) 
	{
		inputArray[i] = *in1++; // populate the inputArray with the input stream
	}
		
	ctrlOut = avg.returnAverage(inputArray);  // get the weighted average and convert it to a control (no longer a signal)
		
	harmonicOut = avg.getHarmonicFrequency();
	
	outlet_float(x->f_ctrlOut, ctrlOut);
	outlet_float(x->f_harmonicOut, harmonicOut);
	
	return (w+4); // the _perform function returns a pointer of the number of dsp elements + 1, see dsp_add()
}

void binavg_tilde_dsp(t_binavg_tilde *x, t_signal **sp)
{
	dsp_add(binavg_tilde_perform, 3, x,
			sp[0]->s_vec, sp[0]->s_n);
}

void *binavg_tilde_new(t_floatarg f)
{
	t_binavg_tilde *x = (t_binavg_tilde *)pd_new(binavg_tilde_class);
	
	x->f_binavg = f;
	
	wAverage avg;
	bool blockCheck = 0;
	
	blockCheck = avg.checkBlock( avg.getBlocksize() ); //verify block size is power of 2 and
	// greater than 512
	
	if (!blockCheck)
	{
		post("binavg~ error: Your block size is not large enough for decent results. Change to 512 or greater using the [block~] object. 2048 or 4096 is recommended.");
	}//inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
	
	//floatinlet_new (&x->x_obj, &x->f_binavg);
	x->f_ctrlOut = outlet_new(&x->x_obj, &s_float);
	x->f_harmonicOut = outlet_new(&x->x_obj, &s_float);
	
	
	return (void *)x;
}

// note that the gensym("") must use the product name (e.g name of .pd_darwin or pd_linux file) to be cross compatible
void binavg_tilde_setup(void) {
	binavg_tilde_class = class_new(gensym("binavg~"),
								(t_newmethod)binavg_tilde_new,
								0, sizeof(t_binavg_tilde),
								CLASS_DEFAULT, 
								A_DEFFLOAT, 0);
	
	
	class_addmethod(binavg_tilde_class,
					(t_method)binavg_tilde_dsp, gensym("dsp"), A_DEFFLOAT, 0); 
	CLASS_MAINSIGNALIN(binavg_tilde_class, t_binavg_tilde, f);
}

	
#ifdef __cplusplus
}
#endif
