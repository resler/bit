//
//  main.cpp
//  eigthBit
//
//  This project will crush the bit depth of the incoming signal

//  Created by Robert Esler on 7/29/14.
//  Copyright (c) 2014 Robert Esler. All rights reserved.
//

#include "m_pd.h"
#include <cmath>
#define BITDEPTH 16

#ifdef __cplusplus
extern "C" {
#endif

static t_class *eightbit_tilde_class;

typedef struct _eightbit_tilde {
    t_object  x_obj;
    t_float bitDepth;
    t_float bitsPerCycle;
    t_sample freq;
    t_sample theOutput;
    t_float samplerate;
    t_float samplesPerCycle;
    double numerator;
    double factor;
    int counter;
    int bitCounter;
    double cosine = 0;
    t_inlet *x_in2;
    t_outlet*x_out;
} t_eightbit_tilde;

void eightbit_tilde_float(t_eightbit_tilde *x, t_sample *f) {
    
    
        x->freq = *f;
    
        if (x->freq == 0)
            x->factor = 0;
        else
        {
            x->samplesPerCycle = x->samplerate/x->freq;
            x->factor = (2*3.1415915926)/x->samplesPerCycle;
        }
    
}
    
    /*the _perform function is where the DSP code lives.*/
t_int *eightbit_tilde_perform(t_int *w)
{
    t_eightbit_tilde *x = (t_eightbit_tilde *)(w[1]);
    t_sample *in1 = (t_sample *)(w[2]);
    t_sample  *out =    (t_sample *)(w[3]);
    int          n =           (int)(w[4]);
   
    double intPart;
    double fracPart;
    double temp = 0.0;
    eightbit_tilde_float(x, in1++);
    
    if(x->bitDepth == 0)
        x->bitDepth = 1;
 
    /*DSP loop*/
    while (n--)
    {
        x->cosine = (cos(x->counter++*(x->factor)));
        x->counter %= (int)x->samplesPerCycle+1;
       
        temp = x->cosine;
        temp *= x->bitDepth;
        
        fracPart = modf(temp, &intPart);
        
        x->theOutput = intPart * (1/x->bitDepth);
        
        *out++ = x->theOutput;
        
    }
    
    return (w+5);
}

    /*This is the dsp vector chain*/
void eightbit_tilde_dsp(t_eightbit_tilde *x, t_signal **sp)
{
    dsp_add(eightbit_tilde_perform, 4, x,
            sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
}

    /*The Pd deconstructor*/
void eightbit_tilde_free(t_eightbit_tilde *x)
{
    inlet_free(x->x_in2);
    outlet_free(x->x_out);
}
    
/*The Pd constructor*/
void *eightbit_tilde_new(t_floatarg f, t_floatarg f2)
{
    t_eightbit_tilde *x = (t_eightbit_tilde *)pd_new(eightbit_tilde_class);
    
    x->freq = f;
    x->bitDepth = f2;
    x->bitCounter = 1;
    x->samplerate = sys_getsr();
    
    x->counter = 1;
    x->theOutput = 0;
    
    x->x_in2 = floatinlet_new(&x->x_obj, &x->bitDepth);
    x->x_out = outlet_new(&x->x_obj, &s_signal);
    
    return (void *)x;
}

    /*This is Pd jargon to instantiate the object into memory.*/
void eightbit_tilde_setup(void) {
    eightbit_tilde_class = class_new(gensym("eightbit~"),
                                (t_newmethod)eightbit_tilde_new,
                                (t_method)eightbit_tilde_free, sizeof(t_eightbit_tilde),
                                CLASS_DEFAULT,
                                A_DEFFLOAT, A_DEFFLOAT, 0);
    
    class_addmethod(eightbit_tilde_class,
                    (t_method)eightbit_tilde_dsp, gensym("dsp"),A_DEFFLOAT, 0);
   // class_addfloat(eightbit_tilde_class, (t_method)eightbit_tilde_float);
    class_sethelpsymbol(eightbit_tilde_class, gensym("help-eightbit~"));
    
    CLASS_MAINSIGNALIN(eightbit_tilde_class, t_eightbit_tilde, freq);
}

#ifdef __cplusplus
}
#endif
